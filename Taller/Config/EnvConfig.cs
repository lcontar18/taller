﻿namespace Taller.Config
{
    using System;

    public class EnvConfig
    {
        public static string ConnString { get => Environment.GetEnvironmentVariable("DATABASE_CONNECTION_STRING"); }

        public static bool IsRunningInContainer { get => RunningInContainer(); }

        private static bool RunningInContainer()
        {
            bool runningInContainer;
            bool.TryParse(
                Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER"),
                out runningInContainer);
            return runningInContainer;
        }
    }
}