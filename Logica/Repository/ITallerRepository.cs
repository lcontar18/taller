﻿namespace Taller.Logica.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Taller.Modelos.Models;
    using static Taller.Persistencia.RepuestoDataManager;

    public interface ITallerRepository
    {
        /// <summary>
        ///  Get a List of Repuestos.
        /// </summary>
        /// <returns>A List of Repuestos gorup by Marca and Modelo</returns>
        List<Tuple<string, string, string, int>> GetMostUsedRepuestoByMarcaModelo();

        /// <summary>
        ///  Get a List of Montos.
        /// </summary>
        /// <returns>A List of Montos gorup by Marca and Modelo</returns>
        List<Tuple<string, string, decimal>> GetAverageMontoTotalPresupuestosByMarcaModelo();

        /// <summary>
        ///  Get a List of Presupuesto's Montos Totales .
        /// </summary>
        /// <returns>AList of Repuestos gorup by Marca and Modelo</returns>
        Tuple<decimal, decimal> GetSumMontoTotalPresupuestosForAutosAndMotos();

        /// <summary>
        ///  Inserts a Vehiculo into the DB using marca, modelo, tipo, patente, cilindrada or cantidadpuertas.
        /// </summary>
        /// <param name="marca">Vehiculo's Marca.</param>
        /// <param name="modelo">Vehiculo's Modelo.</param>
        /// <param name="tipoVehiculo">Vehiculo's Type.</param>
        /// <param name="patente">Vehiculo's Patente.</param>
        /// <param name="cilindrada">Moto's Cilindrada.</param>
        /// <param name="tipoAutomovil">Automovil's Type.</param>
        /// <param name="cantidadPuertas">Automovil's Q of doors.</param>
        public void AgregarVehiculo(string marca, string modelo, TipoVehiculo tipoVehiculo, string patente = null, string cilindrada = null, AutomovilTipo? tipoAutomovil = null, short? cantidadPuertas = null);

        /// <summary>
        ///  Generates a presupuesto and inserts data into the DB using Patente, Nombre, Apellido and Email.
        /// </summary>
        /// <param name="nombre">Person's Nombre.</param>
        /// <param name="apellido">Person's Apellido.</param>
        /// <param name="email">Person's Email</param>
        /// <param name="patente">Vehiculo's Patente.</param>
        public void GenerarPresupuesto(string nombre, string apellido, string email, string patente);

        /// <summary>
        ///  Get a Presupuesto using Vehiculo's Patente.
        /// </summary>
        /// <param name="patente">Vehiculo's Patente.</param>
        /// <returns>A Presupuesto</returns>
        public Presupuesto ObtenerPresupuestoPorPatente(string patente);
    }
}
