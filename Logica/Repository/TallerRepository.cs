﻿namespace Taller.Logica.Repository
{
    using System;
    using System.Collections.Generic;
    using Taller.Modelos.Models;
    using Taller.Persistencia;

    public class TallerRepository : ITallerRepository
    {
        private readonly RepuestoDataManager repuestoDataManager;

        public TallerRepository(RepuestoDataManager dataManager)
        {
            this.repuestoDataManager = dataManager ?? throw new ArgumentNullException(nameof(dataManager));
        }

        // Método para obtener el repuesto más utilizado por Marca/Modelo en las reparaciones realizadas
        /// <inheritdoc/>
        public List<Tuple<string, string, string, int>> GetMostUsedRepuestoByMarcaModelo()
        {
            return this.repuestoDataManager.GetMostUsedRepuestoByMarcaModelo();
        }

        // Método para obtener el promedio del Monto Total de Presupuestos por Marca/Modelo
        /// <inheritdoc/>
        public List<Tuple<string, string, decimal>> GetAverageMontoTotalPresupuestosByMarcaModelo()
        {
            return this.repuestoDataManager.GetTotalPresupuestosByMarcaModelo();
        }

        // Método para obtener la sumatoria del Monto Total de Presupuestos para Autos y para Motos
        /// <inheritdoc/>
        public Tuple<decimal, decimal> GetSumMontoTotalPresupuestosForAutosAndMotos()
        {
            return this.repuestoDataManager.GetTotalPresupuestosForAutosAndMotos();
        }

        // Método para agregar un nuevo vehículo a la base de datos
        /// <inheritdoc/>
        public void AgregarVehiculo(string marca, string modelo,  TipoVehiculo tipoVehiculo, string patente = null, string cilindrada = null, AutomovilTipo? tipoAutomovil = null, short? cantidadPuertas = null)
        {
            this.repuestoDataManager.AgregarVehiculo(marca, modelo, tipoVehiculo, patente, cilindrada, tipoAutomovil, cantidadPuertas);
        }

        // Método para generar un presupuesto en base a los datos del cliente y vehículo
        /// <inheritdoc/>
        public void GenerarPresupuesto(string nombre, string apellido, string email, string patente)
        {
            this.repuestoDataManager.GenerarPresupuesto(nombre, apellido, email, patente);
        }

        // Método para obtener un presupuesto por la patente del vehículo
        /// <inheritdoc/>
        public Presupuesto ObtenerPresupuestoPorPatente(string patente)
        {
            return this.repuestoDataManager.ObtenerPresupuestoPorPatente(patente);
        }

    }
}
