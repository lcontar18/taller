namespace Taller.Modelos.Models
{
    using System.Collections.Generic;

    // Clase Desperfecto para representar los valores de tiempo y valores
    public partial class Desperfecto
    {
        public long Id { get; set; }

        public long? IdPresupuesto { get; set; }

        public string Descripcion { get; set; }

        public decimal? ManoDeObra { get; set; }

        public int? Tiempo { get; set; }

        public virtual Presupuesto IdPresupuestoNavigation { get; set; }
    }
}