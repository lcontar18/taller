namespace Taller.Modelos.Models
{
    using System;

    // Clase Moto que hereda de Vehiculo
    public partial class Moto : Vehiculo
    {
        public long Id { get; set; }

        public long? IdVehiculo { get; set; }

        public string Cilindrada { get; set; }

        public virtual Vehiculo IdVehiculoNavigation { get; set; }
    }
}