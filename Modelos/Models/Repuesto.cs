namespace Taller.Modelos.Models
{
    using System;
    using System.Collections.Generic;

    // Clase Repuesto para representar los repuestos disponibles en el taller
    public partial class Repuesto
    {
        public long Id { get; set; }

        public string Nombre { get; set; }

        public decimal? Precio { get; set; }
    }
}