namespace Taller.Modelos.Models
{
    using System;
    using System.Collections.Generic;

    // Clase Automovil que hereda de Vehiculo

    public enum TipoVehiculo
    {
        Moto,
        Automovil
    }
public abstract class Vehiculo
    {
        public long Id { get; set; }

        public string Marca { get; set; }

        public string Modelo { get; set; }

        public string Patente { get; set; }
    }
}