namespace Taller.Modelos.Models
{
    public partial class DesperfectoRepuesto
    {
        public long Id { get; set; }

        public long? IdDesperfecto { get; set; }

        public long? IdRepuesto { get; set; }

        public virtual Desperfecto IdDesperfectoNavigation { get; set; }

        public virtual Repuesto IdRepuestoNavigation { get; set; }
    }
}