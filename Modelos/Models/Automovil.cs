namespace Taller.Modelos.Models
{
    using System;

    // Enumerador para el Tipo de Automóvil
    public enum AutomovilTipo
    {
        Compacto,
        Sedan,
        Monovolumen,
        Utilitario,
        Lujo
    }

    // Clase Automovil que hereda de Vehiculo
    public partial class Automovil : Vehiculo
    {
        public long Id { get; set; }

        public long? IdVehiculo { get; set; }

        public short? Tipo { get; set; }

        public short? CantidadPuertas { get; set; }

        public virtual Vehiculo IdVehiculoNavigation { get; set; }
    }
}