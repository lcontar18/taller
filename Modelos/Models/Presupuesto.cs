namespace Taller.Modelos.Models
{
    using System;
    using System.Collections.Generic;

    // Clase Presupuesto para representar los valores
    public partial class Presupuesto
    {
        public Presupuesto()
        {
            this.Desperfectos = new HashSet<Desperfecto>();
        }

        public long Id { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Email { get; set; }

        public decimal? Total { get; set; }

        public long? IdVehiculo { get; set; }

        public virtual Vehiculo IdVehiculoNavigation { get; set; }

        public virtual ICollection<Desperfecto> Desperfectos { get; set; }

        public List<DetallesPresupuesto> DetallesPresupuesto { get; set; }
    }

    public class DetallesPresupuesto
    {
    public long DesperfectoId { get; set; }
    public string Descripcion { get; set; }
    public decimal ManoDeObra { get; set; }
    public int Tiempo { get; set; }
    public string RepuestoNombre { get; set; }
    public decimal RepuestoPrecio { get; set; }
    }
}