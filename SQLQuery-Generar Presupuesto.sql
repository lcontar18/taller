CREATE PROCEDURE GenerarPresupuesto
    @Nombre NVARCHAR(100),
    @Apellido NVARCHAR(100),
    @Email NVARCHAR(100),
    @Patente NVARCHAR(10)
AS
BEGIN
    -- Obtener el ID del veh�culo en base a la patente
    DECLARE @IdVehiculo BIGINT
    SELECT @IdVehiculo = Id FROM Vehiculo WHERE Patente = @Patente

    IF @IdVehiculo IS NULL
    BEGIN
        RAISERROR('No se encontr� ning�n veh�culo con la patente proporcionada.', 16, 1)
        RETURN
    END

    -- Calcular el costo total del presupuesto
    DECLARE @CostoTotal DECIMAL(18, 2) = 0

    -- Generar un n�mero aleatorio de desperfectos entre 1 y 5
    DECLARE @NumeroDesperfectos INT = (ABS(CHECKSUM(NEWID())) % 5) + 1

    -- Insertar el registro en la tabla Presupuesto
    INSERT INTO Presupuesto (Nombre, Apellido, Email, Total, IdVehiculo)
    VALUES (@Nombre, @Apellido, @Email, @CostoTotal, @IdVehiculo)

    DECLARE @IdPresupuesto BIGINT = SCOPE_IDENTITY()

    -- Generar y asignar los desperfectos al presupuesto
    WHILE @NumeroDesperfectos > 0
    BEGIN
        -- Generar valores aleatorios para descripci�n, tiempo y mano de obra
        DECLARE @Descripcion NVARCHAR(500) = 'Desperfecto ' + CAST(@NumeroDesperfectos AS NVARCHAR(10))
        DECLARE @Tiempo INT = (ABS(CHECKSUM(NEWID())) % 10) + 1
        DECLARE @ManoDeObra DECIMAL(18, 2) = 130

        -- Asignar un repuesto aleatorio a cada desperfecto
        DECLARE @IdRepuesto BIGINT
        SELECT TOP 1 @IdRepuesto = Id FROM Repuesto ORDER BY NEWID()

        -- Obtener el precio del repuesto asignado al desperfecto
        DECLARE @PrecioRepuesto DECIMAL(18, 2)
        SELECT @PrecioRepuesto = Precio FROM Repuesto WHERE Id = @IdRepuesto

        -- Insertar el desperfecto en la tabla Desperfecto
        INSERT INTO Desperfecto (IdPresupuesto, Descripcion, ManoDeObra, Tiempo)
        VALUES (@IdPresupuesto, @Descripcion, @ManoDeObra, @Tiempo)

        -- Insertar el repuesto asignado al desperfecto en la tabla DesperfectoRepuesto
        INSERT INTO DesperfectoRepuesto (IdDesperfecto, IdRepuesto)
        VALUES (SCOPE_IDENTITY(), @IdRepuesto)

        -- Agregar el costo de mano de obra multiplicado por el tiempo del desperfecto al costo total
        SET @CostoTotal = @CostoTotal + (@ManoDeObra * @Tiempo) + @PrecioRepuesto

        SET @NumeroDesperfectos = @NumeroDesperfectos - 1
    END

    -- Actualizar el costo total del presupuesto
    UPDATE Presupuesto SET Total = @CostoTotal WHERE Id = @IdPresupuesto
END
