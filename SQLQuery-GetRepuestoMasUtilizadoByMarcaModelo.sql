CREATE PROCEDURE dbo.GetRepuestoMasUtilizadoByMarcaModelo
    @Marca NVARCHAR(50),
    @Modelo NVARCHAR(50)
AS
BEGIN
    -- C�digo para obtener el repuesto m�s utilizado por Marca/Modelo en las reparaciones realizadas
    SELECT TOP 1 R.Nombre AS DescripcionRepuesto, COUNT(DR.IdRepuesto) AS CantidadVecesUsado
    FROM Presupuesto AS P
    INNER JOIN Desperfecto AS D ON P.Id = D.IdPresupuesto
    INNER JOIN DesperfectoRepuesto AS DR ON D.Id = DR.IdDesperfecto
    INNER JOIN Repuesto AS R ON DR.IdRepuesto = R.Id
    INNER JOIN Vehiculo AS V ON P.IdVehiculo = V.Id
    WHERE V.Marca = @Marca AND V.Modelo = @Modelo
    GROUP BY R.Nombre
    ORDER BY COUNT(DR.IdRepuesto) DESC
END
