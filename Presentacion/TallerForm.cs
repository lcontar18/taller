﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Taller.Logica.Repository;
using Taller.Modelos.Models;
using Taller.Persistencia;

namespace Presentacion
{
    public partial class TallerForm : Form
    {
        private readonly ITallerRepository tallerRepository;
        private readonly RepuestoDataManager repuestoDataManager;

        public TallerForm()
        {
            InitializeComponent();
            var connectionString = "Server=DESKTOP-VCNURLD\\SQLEXPRESS;Database=DiWorks;Trusted_Connection=True";
            this.repuestoDataManager = new RepuestoDataManager(connectionString);
            this.tallerRepository = new TallerRepository(this.repuestoDataManager);

            // Configurar el desplegable para el tipo de automóvil
            cmbTipoAutomovil.DataSource = Enum.GetValues(typeof(AutomovilTipo));
            cmbTipoAutomovil.SelectedIndex = 0; // Establecer Compacto como opción predeterminada
            cmbTipoVehiculo.DataSource = Enum.GetValues(typeof(TipoVehiculo));
            cmbTipoVehiculo.SelectedIndex = 1; // Establecer Automovil como opción predeterminada

            // Llamar al método para mostrar los campos adecuados según el tipo de vehículo inicial
            MostrarCamposSegunTipoVehiculo();

            // Configurar el DataGridView
            dataGridViewPresupuesto.AutoGenerateColumns = false;
            dataGridViewMainPresupuesto.AutoGenerateColumns = false;

            // Agregar las columnas manualmente
            dataGridViewMainPresupuesto.Columns.Add("Nombre", "Nombre");
            dataGridViewMainPresupuesto.Columns.Add("Apellido", "Apellido");
            dataGridViewMainPresupuesto.Columns.Add("Email", "Email");
            dataGridViewMainPresupuesto.Columns.Add("Total", "Total");

            // Agregar las columnas manualmente
            dataGridViewPresupuesto.Columns.Add("Descripcion", "Descripción de Desperfecto");
            dataGridViewPresupuesto.Columns.Add("ManoDeObra", "Costo Mano de Obra");
            dataGridViewPresupuesto.Columns.Add("Tiempo", "Dias de Trabajo");
            dataGridViewPresupuesto.Columns.Add("RepuestoNombre", "Nombre de Repuesto");
            dataGridViewPresupuesto.Columns.Add("RepuestoPrecio", "Precio Repuesto");

            // Configurar el modo de tamaño de las columnas para que ocupen todo el ancho disponible
            dataGridViewPresupuesto.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewMainPresupuesto.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void btnAgregarVehiculo_Click(object sender, EventArgs e)
        {
            try
            {
                string marca = txtMarca.Text;
                string modelo = txtModelo.Text;
                TipoVehiculo tipoVehiculo = (TipoVehiculo)cmbTipoVehiculo.SelectedItem;
                string patente = txtPatente.Text;
                AutomovilTipo tipoAutomovil = (AutomovilTipo)cmbTipoAutomovil.SelectedItem;
                string cilindrada = txtCilindrada.Text;
                short? cantidadPuertas = null;

                // Verificar si el tipo de vehículo seleccionado es Automovil para obtener la cantidad de puertas
                if (tipoVehiculo == TipoVehiculo.Automovil)
                {
                    cantidadPuertas = Convert.ToInt16(txtCantidadPuertas.Text);
                }

                this.tallerRepository.AgregarVehiculo(marca, modelo, tipoVehiculo, patente, cilindrada, tipoAutomovil, cantidadPuertas);
                MessageBox.Show("¡Vehículo agregado correctamente!");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al agregar el vehículo: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbTipoVehiculo_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarCamposSegunTipoVehiculo();
        }

        private void MostrarCamposSegunTipoVehiculo()
        {
            TipoVehiculo tipoVehiculo = (TipoVehiculo)cmbTipoVehiculo.SelectedItem;

            if (tipoVehiculo == TipoVehiculo.Automovil)
            {
                txtCilindrada.Visible = false;
                lblCilindrada.Visible = false;
                txtCantidadPuertas.Visible = true;
                lblCantidadPuertas.Visible = true;
            }
            else if (tipoVehiculo == TipoVehiculo.Moto)
            {
                txtCantidadPuertas.Visible = false;
                lblCantidadPuertas.Visible = false;
                txtCilindrada.Visible = true;
                lblCilindrada.Visible = true;
            }
        }

        private void btnGenerarPresupuesto_Click(object sender, EventArgs e)
        {
            try
            {
                // Obtener los datos ingresados por el cliente
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;
                string email = txtEmail.Text;
                string patente = txtPatente2.Text; // Aquí cambiamos a txtPatente2

                // Validar que se haya ingresado la patente
                if (string.IsNullOrEmpty(patente))
                {
                    MessageBox.Show("Por favor, ingrese una patente válida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Generar el presupuesto llamando al método GenerarPresupuesto del RepuestoDataManager
                this.repuestoDataManager.GenerarPresupuesto(nombre, apellido, email, patente);

                MessageBox.Show("Presupuesto generado correctamente.", "Éxito", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al generar el presupuesto: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void btnBuscarPresupuesto_Click(object sender, EventArgs e)
        {
            try
            {
                // Obtener la patente ingresada por el cliente
                string patente = txtBuscarPatente.Text;

                // Validar que se haya ingresado la patente
                if (string.IsNullOrEmpty(patente))
                {
                    MessageBox.Show("Por favor, ingrese una patente válida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Buscar el presupuesto asociado a la patente
                Presupuesto presupuesto = this.repuestoDataManager.ObtenerPresupuestoPorPatente(patente);

                if (presupuesto == null)
                {
                    MessageBox.Show("No se encontró ningún presupuesto asociado a la patente proporcionada.", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dataGridViewMainPresupuesto.Rows.Clear();
                    dataGridViewMainPresupuesto.Rows.Add(
                        presupuesto.Nombre,
                        presupuesto.Apellido,
                        presupuesto.Email,
                        presupuesto.Total.ToString()
                    );

                    MessageBox.Show("Presupuesto Encontrado", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    // Agregar las filas con los detalles del presupuesto
                    dataGridViewPresupuesto.Rows.Clear();
                    foreach (DetallesPresupuesto detalle in presupuesto.DetallesPresupuesto)
                    {
                        dataGridViewPresupuesto.Rows.Add(
                            detalle.Descripcion,
                            detalle.ManoDeObra.ToString("C2"),
                            detalle.Tiempo.ToString(),
                            detalle.RepuestoNombre,
                            detalle.RepuestoPrecio.ToString("C2")
                        );
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error al buscar el presupuesto: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
