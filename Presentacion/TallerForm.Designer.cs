﻿namespace Presentacion
{
    partial class TallerForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtMarca = new System.Windows.Forms.TextBox();
            txtModelo = new System.Windows.Forms.TextBox();
            cmbTipoVehiculo = new System.Windows.Forms.ComboBox();
            txtPatente = new System.Windows.Forms.TextBox();
            cmbTipoAutomovil = new System.Windows.Forms.ComboBox();
            txtCilindrada = new System.Windows.Forms.TextBox();
            txtCantidadPuertas = new System.Windows.Forms.TextBox();
            lblMarca = new System.Windows.Forms.Label();
            lblModelo = new System.Windows.Forms.Label();
            lblTipoVehiculo = new System.Windows.Forms.Label();
            lblPatente = new System.Windows.Forms.Label();
            lblTipoAutomovil = new System.Windows.Forms.Label();
            lblCilindrada = new System.Windows.Forms.Label();
            lblCantidadPuertas = new System.Windows.Forms.Label();
            btnAgregarVehiculo = new System.Windows.Forms.Button();
            button1 = new System.Windows.Forms.Button();
            txtNombre = new System.Windows.Forms.TextBox();
            lblNombre = new System.Windows.Forms.Label();
            lblApellido = new System.Windows.Forms.Label();
            lblEmail = new System.Windows.Forms.Label();
            lblPatente2 = new System.Windows.Forms.Label();
            txtApellido = new System.Windows.Forms.TextBox();
            txtEmail = new System.Windows.Forms.TextBox();
            txtPatente2 = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            button2 = new System.Windows.Forms.Button();
            txtBuscarPatente = new System.Windows.Forms.TextBox();
            lblBuscarPatente = new System.Windows.Forms.Label();
            dataGridViewPresupuesto = new System.Windows.Forms.DataGridView();
            dataGridViewMainPresupuesto = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)dataGridViewPresupuesto).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dataGridViewMainPresupuesto).BeginInit();
            SuspendLayout();
            // 
            // txtMarca
            // 
            txtMarca.AccessibleDescription = "Marca";
            txtMarca.AccessibleName = "Marca";
            txtMarca.Location = new System.Drawing.Point(36, 61);
            txtMarca.Name = "txtMarca";
            txtMarca.Size = new System.Drawing.Size(100, 23);
            txtMarca.TabIndex = 0;
            txtMarca.Text = "Ingrese Marca";
            // 
            // txtModelo
            // 
            txtModelo.Location = new System.Drawing.Point(142, 61);
            txtModelo.Name = "txtModelo";
            txtModelo.Size = new System.Drawing.Size(100, 23);
            txtModelo.TabIndex = 1;
            txtModelo.Text = "Ingrese Modelo";
            // 
            // cmbTipoVehiculo
            // 
            cmbTipoVehiculo.FormattingEnabled = true;
            cmbTipoVehiculo.Location = new System.Drawing.Point(248, 61);
            cmbTipoVehiculo.Name = "cmbTipoVehiculo";
            cmbTipoVehiculo.Size = new System.Drawing.Size(121, 23);
            cmbTipoVehiculo.TabIndex = 2;
            cmbTipoVehiculo.SelectedIndexChanged += cmbTipoVehiculo_SelectedIndexChanged;
            // 
            // txtPatente
            // 
            txtPatente.Location = new System.Drawing.Point(375, 61);
            txtPatente.Name = "txtPatente";
            txtPatente.Size = new System.Drawing.Size(100, 23);
            txtPatente.TabIndex = 3;
            txtPatente.Text = "Ingrese la Patente";
            // 
            // cmbTipoAutomovil
            // 
            cmbTipoAutomovil.FormattingEnabled = true;
            cmbTipoAutomovil.Location = new System.Drawing.Point(481, 61);
            cmbTipoAutomovil.Name = "cmbTipoAutomovil";
            cmbTipoAutomovil.Size = new System.Drawing.Size(121, 23);
            cmbTipoAutomovil.TabIndex = 4;
            // 
            // txtCilindrada
            // 
            txtCilindrada.Location = new System.Drawing.Point(608, 61);
            txtCilindrada.Name = "txtCilindrada";
            txtCilindrada.Size = new System.Drawing.Size(138, 23);
            txtCilindrada.TabIndex = 5;
            txtCilindrada.Text = "Cilindrada de Moto";
            txtCilindrada.Visible = false;
            // 
            // txtCantidadPuertas
            // 
            txtCantidadPuertas.Location = new System.Drawing.Point(752, 61);
            txtCantidadPuertas.Name = "txtCantidadPuertas";
            txtCantidadPuertas.Size = new System.Drawing.Size(160, 23);
            txtCantidadPuertas.TabIndex = 6;
            txtCantidadPuertas.Text = "Cantidad de Puertas de Auto";
            txtCantidadPuertas.Visible = false;
            // 
            // lblMarca
            // 
            lblMarca.AutoSize = true;
            lblMarca.Location = new System.Drawing.Point(36, 43);
            lblMarca.Name = "lblMarca";
            lblMarca.Size = new System.Drawing.Size(40, 15);
            lblMarca.TabIndex = 8;
            lblMarca.Text = "Marca";
            // 
            // lblModelo
            // 
            lblModelo.AutoSize = true;
            lblModelo.Location = new System.Drawing.Point(142, 43);
            lblModelo.Name = "lblModelo";
            lblModelo.Size = new System.Drawing.Size(48, 15);
            lblModelo.TabIndex = 9;
            lblModelo.Text = "Modelo";
            // 
            // lblTipoVehiculo
            // 
            lblTipoVehiculo.AutoSize = true;
            lblTipoVehiculo.Location = new System.Drawing.Point(248, 43);
            lblTipoVehiculo.Name = "lblTipoVehiculo";
            lblTipoVehiculo.Size = new System.Drawing.Size(75, 15);
            lblTipoVehiculo.TabIndex = 10;
            lblTipoVehiculo.Text = "TipoVehiculo";
            // 
            // lblPatente
            // 
            lblPatente.AutoSize = true;
            lblPatente.Location = new System.Drawing.Point(375, 43);
            lblPatente.Name = "lblPatente";
            lblPatente.Size = new System.Drawing.Size(47, 15);
            lblPatente.TabIndex = 11;
            lblPatente.Text = "Patente";
            // 
            // lblTipoAutomovil
            // 
            lblTipoAutomovil.AutoSize = true;
            lblTipoAutomovil.Location = new System.Drawing.Point(481, 43);
            lblTipoAutomovil.Name = "lblTipoAutomovil";
            lblTipoAutomovil.Size = new System.Drawing.Size(86, 15);
            lblTipoAutomovil.TabIndex = 12;
            lblTipoAutomovil.Text = "TipoAutomovil";
            // 
            // lblCilindrada
            // 
            lblCilindrada.AutoSize = true;
            lblCilindrada.Location = new System.Drawing.Point(608, 43);
            lblCilindrada.Name = "lblCilindrada";
            lblCilindrada.Size = new System.Drawing.Size(61, 15);
            lblCilindrada.TabIndex = 13;
            lblCilindrada.Text = "Cilindrada";
            lblCilindrada.Visible = false;
            // 
            // lblCantidadPuertas
            // 
            lblCantidadPuertas.AutoSize = true;
            lblCantidadPuertas.Location = new System.Drawing.Point(752, 43);
            lblCantidadPuertas.Name = "lblCantidadPuertas";
            lblCantidadPuertas.Size = new System.Drawing.Size(94, 15);
            lblCantidadPuertas.TabIndex = 14;
            lblCantidadPuertas.Text = "CantidadPuertas";
            lblCantidadPuertas.Visible = false;
            // 
            // btnAgregarVehiculo
            // 
            btnAgregarVehiculo.Location = new System.Drawing.Point(36, 106);
            btnAgregarVehiculo.Name = "btnAgregarVehiculo";
            btnAgregarVehiculo.Size = new System.Drawing.Size(133, 23);
            btnAgregarVehiculo.TabIndex = 15;
            btnAgregarVehiculo.Text = "Agregar Vehículo";
            btnAgregarVehiculo.UseVisualStyleBackColor = true;
            btnAgregarVehiculo.Click += btnAgregarVehiculo_Click;
            // 
            // button1
            // 
            button1.Location = new System.Drawing.Point(36, 230);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(131, 23);
            button1.TabIndex = 16;
            button1.Text = "Generar Presupuesto";
            button1.UseVisualStyleBackColor = true;
            button1.Click += btnGenerarPresupuesto_Click;
            // 
            // txtNombre
            // 
            txtNombre.Location = new System.Drawing.Point(36, 187);
            txtNombre.Name = "txtNombre";
            txtNombre.Size = new System.Drawing.Size(142, 23);
            txtNombre.TabIndex = 17;
            txtNombre.Text = "Ingrese su Nombre";
            // 
            // lblNombre
            // 
            lblNombre.AutoSize = true;
            lblNombre.Location = new System.Drawing.Point(36, 169);
            lblNombre.Name = "lblNombre";
            lblNombre.Size = new System.Drawing.Size(51, 15);
            lblNombre.TabIndex = 18;
            lblNombre.Text = "Nombre";
            // 
            // lblApellido
            // 
            lblApellido.AutoSize = true;
            lblApellido.Location = new System.Drawing.Point(184, 169);
            lblApellido.Name = "lblApellido";
            lblApellido.Size = new System.Drawing.Size(51, 15);
            lblApellido.TabIndex = 19;
            lblApellido.Text = "Apellido";
            // 
            // lblEmail
            // 
            lblEmail.AutoSize = true;
            lblEmail.Location = new System.Drawing.Point(338, 169);
            lblEmail.Name = "lblEmail";
            lblEmail.Size = new System.Drawing.Size(36, 15);
            lblEmail.TabIndex = 20;
            lblEmail.Text = "Email";
            // 
            // lblPatente2
            // 
            lblPatente2.AutoSize = true;
            lblPatente2.Location = new System.Drawing.Point(498, 169);
            lblPatente2.Name = "lblPatente2";
            lblPatente2.Size = new System.Drawing.Size(47, 15);
            lblPatente2.TabIndex = 21;
            lblPatente2.Text = "Patente";
            // 
            // txtApellido
            // 
            txtApellido.Location = new System.Drawing.Point(184, 187);
            txtApellido.Name = "txtApellido";
            txtApellido.Size = new System.Drawing.Size(137, 23);
            txtApellido.TabIndex = 22;
            txtApellido.Text = "Ingrese su Apellido";
            // 
            // txtEmail
            // 
            txtEmail.Location = new System.Drawing.Point(338, 187);
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new System.Drawing.Size(154, 23);
            txtEmail.TabIndex = 23;
            txtEmail.Text = "Ingrese su Email";
            // 
            // txtPatente2
            // 
            txtPatente2.Location = new System.Drawing.Point(498, 187);
            txtPatente2.Name = "txtPatente2";
            txtPatente2.Size = new System.Drawing.Size(198, 23);
            txtPatente2.TabIndex = 24;
            txtPatente2.Text = "Ingrese La patente del Vehiculo";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(34, 144);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(126, 15);
            label1.TabIndex = 25;
            label1.Text = "Genera tu presupuesto";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(36, 9);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(107, 15);
            label2.TabIndex = 26;
            label2.Text = "Agrega tu Vehiculo";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(36, 285);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(110, 15);
            label3.TabIndex = 27;
            label3.Text = "Buscar Presupuesto";
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(38, 379);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(152, 23);
            button2.TabIndex = 28;
            button2.Text = "Buscar Presupuesto";
            button2.UseVisualStyleBackColor = true;
            button2.Click += btnBuscarPresupuesto_Click;
            // 
            // txtBuscarPatente
            // 
            txtBuscarPatente.Location = new System.Drawing.Point(36, 332);
            txtBuscarPatente.Name = "txtBuscarPatente";
            txtBuscarPatente.Size = new System.Drawing.Size(100, 23);
            txtBuscarPatente.TabIndex = 29;
            txtBuscarPatente.Text = "Buscar Patente";
            // 
            // lblBuscarPatente
            // 
            lblBuscarPatente.AutoSize = true;
            lblBuscarPatente.Location = new System.Drawing.Point(38, 314);
            lblBuscarPatente.Name = "lblBuscarPatente";
            lblBuscarPatente.Size = new System.Drawing.Size(47, 15);
            lblBuscarPatente.TabIndex = 30;
            lblBuscarPatente.Text = "Patente";
            // 
            // dataGridViewPresupuesto
            // 
            dataGridViewPresupuesto.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewPresupuesto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewPresupuesto.Location = new System.Drawing.Point(38, 424);
            dataGridViewPresupuesto.Name = "dataGridViewPresupuesto";
            dataGridViewPresupuesto.RowTemplate.Height = 25;
            dataGridViewPresupuesto.Size = new System.Drawing.Size(874, 150);
            dataGridViewPresupuesto.TabIndex = 31;
            // 
            // dataGridViewMainPresupuesto
            // 
            dataGridViewMainPresupuesto.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewMainPresupuesto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewMainPresupuesto.Location = new System.Drawing.Point(222, 285);
            dataGridViewMainPresupuesto.Name = "dataGridViewMainPresupuesto";
            dataGridViewMainPresupuesto.RowTemplate.Height = 25;
            dataGridViewMainPresupuesto.Size = new System.Drawing.Size(380, 117);
            dataGridViewMainPresupuesto.TabIndex = 32;
            // 
            // TallerForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(948, 628);
            Controls.Add(dataGridViewMainPresupuesto);
            Controls.Add(dataGridViewPresupuesto);
            Controls.Add(lblBuscarPatente);
            Controls.Add(txtBuscarPatente);
            Controls.Add(button2);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(txtPatente2);
            Controls.Add(txtEmail);
            Controls.Add(txtApellido);
            Controls.Add(lblPatente2);
            Controls.Add(lblEmail);
            Controls.Add(lblApellido);
            Controls.Add(lblNombre);
            Controls.Add(txtNombre);
            Controls.Add(button1);
            Controls.Add(btnAgregarVehiculo);
            Controls.Add(lblCantidadPuertas);
            Controls.Add(lblCilindrada);
            Controls.Add(lblTipoAutomovil);
            Controls.Add(lblPatente);
            Controls.Add(lblTipoVehiculo);
            Controls.Add(lblModelo);
            Controls.Add(lblMarca);
            Controls.Add(txtCantidadPuertas);
            Controls.Add(txtCilindrada);
            Controls.Add(cmbTipoAutomovil);
            Controls.Add(txtPatente);
            Controls.Add(cmbTipoVehiculo);
            Controls.Add(txtModelo);
            Controls.Add(txtMarca);
            Name = "TallerForm";
            Text = "TallerForm";
            ((System.ComponentModel.ISupportInitialize)dataGridViewPresupuesto).EndInit();
            ((System.ComponentModel.ISupportInitialize)dataGridViewMainPresupuesto).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox txtMarca;
        private System.Windows.Forms.TextBox txtModelo;
        private System.Windows.Forms.ComboBox cmbTipoVehiculo;
        private System.Windows.Forms.TextBox txtPatente;
        private System.Windows.Forms.ComboBox cmbTipoAutomovil;
        private System.Windows.Forms.TextBox txtCilindrada;
        private System.Windows.Forms.TextBox txtCantidadPuertas;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblTipoVehiculo;
        private System.Windows.Forms.Label lblPatente;
        private System.Windows.Forms.Label lblTipoAutomovil;
        private System.Windows.Forms.Label lblCilindrada;
        private System.Windows.Forms.Label lblCantidadPuertas;
        private System.Windows.Forms.Button btnAgregarVehiculo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPatente2;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtPatente2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtBuscarPatente;
        private System.Windows.Forms.Label lblBuscarPatente;
        private System.Windows.Forms.DataGridView dataGridViewPresupuesto;
        private System.Windows.Forms.DataGridView dataGridViewMainPresupuesto;
    }
}
