ALTER PROCEDURE ObtenerPresupuestoPorPatente
    @Patente NVARCHAR(10)
AS
BEGIN
    -- Obtener el ID del veh�culo en base a la patente
    DECLARE @IdVehiculo BIGINT
    SELECT @IdVehiculo = Id FROM Vehiculo WHERE Patente = @Patente

    IF @IdVehiculo IS NULL
    BEGIN
        RETURN
    END

    -- Buscar presupuesto asociado al veh�culo
    SELECT
        p.Nombre,
        p.Apellido,
        p.Email,
        p.Total,
        d.Id AS DesperfectoId,
        d.Descripcion,
        d.ManoDeObra,
        d.Tiempo,
        r.Nombre AS RepuestoNombre,
        r.Precio AS RepuestoPrecio
    FROM Presupuesto p
    INNER JOIN Desperfecto d ON p.Id = d.IdPresupuesto
    LEFT JOIN DesperfectoRepuesto dr ON d.Id = dr.IdDesperfecto
    LEFT JOIN Repuesto r ON dr.IdRepuesto = r.Id
    WHERE p.IdVehiculo = @IdVehiculo
END
