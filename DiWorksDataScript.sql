USE [master]
GO
/****** Object:  Database [DiWorks]    Script Date: 1/8/2023 18:59:34 ******/
CREATE DATABASE [DiWorks]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DiWorks', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\DiWorks.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DiWorks_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\DiWorks_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [DiWorks] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DiWorks].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DiWorks] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DiWorks] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DiWorks] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DiWorks] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DiWorks] SET ARITHABORT OFF 
GO
ALTER DATABASE [DiWorks] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DiWorks] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DiWorks] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DiWorks] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DiWorks] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DiWorks] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DiWorks] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DiWorks] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DiWorks] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DiWorks] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DiWorks] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DiWorks] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DiWorks] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DiWorks] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DiWorks] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DiWorks] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DiWorks] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DiWorks] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DiWorks] SET  MULTI_USER 
GO
ALTER DATABASE [DiWorks] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DiWorks] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DiWorks] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DiWorks] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DiWorks] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [DiWorks] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [DiWorks] SET QUERY_STORE = OFF
GO
USE [DiWorks]
GO
/****** Object:  Table [dbo].[Automovil]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Automovil](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdVehiculo] [bigint] NOT NULL,
	[Tipo] [smallint] NULL,
	[CantidadPuertas] [smallint] NULL,
 CONSTRAINT [PK_Automovil] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Desperfecto]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Desperfecto](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdPresupuesto] [bigint] NULL,
	[Descripcion] [varchar](250) NULL,
	[ManoDeObra] [decimal](18, 6) NULL,
	[Tiempo] [int] NULL,
 CONSTRAINT [PK_Desperfecto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DesperfectoRepuesto]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DesperfectoRepuesto](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdDesperfecto] [bigint] NULL,
	[IdRepuesto] [bigint] NULL,
 CONSTRAINT [PK_DesperfectoRepuesto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Moto]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Moto](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdVehiculo] [bigint] NOT NULL,
	[Cilindrada] [varchar](50) NULL,
 CONSTRAINT [PK_Moto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Presupuesto]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Presupuesto](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](150) NULL,
	[Apellido] [varchar](150) NULL,
	[Email] [varchar](200) NULL,
	[Total] [decimal](18, 6) NULL,
	[IdVehiculo] [bigint] NULL,
 CONSTRAINT [PK_Presupuesto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Repuesto]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repuesto](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[Precio] [decimal](18, 6) NULL,
 CONSTRAINT [PK_Repuesto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehiculo]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehiculo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Marca] [varchar](100) NULL,
	[Modelo] [varchar](100) NULL,
	[Patente] [varchar](50) NULL,
 CONSTRAINT [PK_Vehiculo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Automovil]  WITH CHECK ADD  CONSTRAINT [FK_Automovil_Vehiculo] FOREIGN KEY([IdVehiculo])
REFERENCES [dbo].[Vehiculo] ([Id])
GO
ALTER TABLE [dbo].[Automovil] CHECK CONSTRAINT [FK_Automovil_Vehiculo]
GO
ALTER TABLE [dbo].[Desperfecto]  WITH CHECK ADD  CONSTRAINT [FK_Desperfecto_Presupuesto] FOREIGN KEY([IdPresupuesto])
REFERENCES [dbo].[Presupuesto] ([Id])
GO
ALTER TABLE [dbo].[Desperfecto] CHECK CONSTRAINT [FK_Desperfecto_Presupuesto]
GO
ALTER TABLE [dbo].[DesperfectoRepuesto]  WITH CHECK ADD  CONSTRAINT [FK_DesperfectoRepuesto_Desperfecto] FOREIGN KEY([IdDesperfecto])
REFERENCES [dbo].[Desperfecto] ([Id])
GO
ALTER TABLE [dbo].[DesperfectoRepuesto] CHECK CONSTRAINT [FK_DesperfectoRepuesto_Desperfecto]
GO
ALTER TABLE [dbo].[DesperfectoRepuesto]  WITH CHECK ADD  CONSTRAINT [FK_DesperfectoRepuesto_Repuesto] FOREIGN KEY([IdRepuesto])
REFERENCES [dbo].[Repuesto] ([Id])
GO
ALTER TABLE [dbo].[DesperfectoRepuesto] CHECK CONSTRAINT [FK_DesperfectoRepuesto_Repuesto]
GO
ALTER TABLE [dbo].[Moto]  WITH CHECK ADD  CONSTRAINT [FK_Moto_Vehiculo] FOREIGN KEY([IdVehiculo])
REFERENCES [dbo].[Vehiculo] ([Id])
GO
ALTER TABLE [dbo].[Moto] CHECK CONSTRAINT [FK_Moto_Vehiculo]
GO
ALTER TABLE [dbo].[Presupuesto]  WITH CHECK ADD  CONSTRAINT [FK_Presupuesto_Vehiculo] FOREIGN KEY([IdVehiculo])
REFERENCES [dbo].[Vehiculo] ([Id])
GO
ALTER TABLE [dbo].[Presupuesto] CHECK CONSTRAINT [FK_Presupuesto_Vehiculo]
GO
/****** Object:  StoredProcedure [dbo].[GenerarPresupuesto]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerarPresupuesto]
    @Nombre NVARCHAR(100),
    @Apellido NVARCHAR(100),
    @Email NVARCHAR(100),
    @Patente NVARCHAR(10)
AS
BEGIN
    -- Obtener el ID del vehículo en base a la patente
    DECLARE @IdVehiculo BIGINT
    SELECT @IdVehiculo = Id FROM Vehiculo WHERE Patente = @Patente

    IF @IdVehiculo IS NULL
    BEGIN
        RAISERROR('No se encontró ningún vehículo con la patente proporcionada.', 16, 1)
        RETURN
    END

    -- Calcular el costo total del presupuesto
    DECLARE @CostoTotal DECIMAL(18, 2) = 0

    -- Generar un número aleatorio de desperfectos entre 1 y 5
    DECLARE @NumeroDesperfectos INT = (ABS(CHECKSUM(NEWID())) % 5) + 1

    -- Insertar el registro en la tabla Presupuesto
    INSERT INTO Presupuesto (Nombre, Apellido, Email, Total, IdVehiculo)
    VALUES (@Nombre, @Apellido, @Email, @CostoTotal, @IdVehiculo)

    DECLARE @IdPresupuesto BIGINT = SCOPE_IDENTITY()

    -- Generar y asignar los desperfectos al presupuesto
    WHILE @NumeroDesperfectos > 0
    BEGIN
        -- Generar valores aleatorios para descripción, tiempo y mano de obra
        DECLARE @Descripcion NVARCHAR(500) = 'Desperfecto ' + CAST(@NumeroDesperfectos AS NVARCHAR(10))
        DECLARE @Tiempo INT = (ABS(CHECKSUM(NEWID())) % 10) + 1
        DECLARE @ManoDeObra DECIMAL(18, 2) = 130

        -- Asignar un repuesto aleatorio a cada desperfecto
        DECLARE @IdRepuesto BIGINT
        SELECT TOP 1 @IdRepuesto = Id FROM Repuesto ORDER BY NEWID()

        -- Obtener el precio del repuesto asignado al desperfecto
        DECLARE @PrecioRepuesto DECIMAL(18, 2)
        SELECT @PrecioRepuesto = Precio FROM Repuesto WHERE Id = @IdRepuesto

        -- Insertar el desperfecto en la tabla Desperfecto
        INSERT INTO Desperfecto (IdPresupuesto, Descripcion, ManoDeObra, Tiempo)
        VALUES (@IdPresupuesto, @Descripcion, @ManoDeObra, @Tiempo)

        -- Insertar el repuesto asignado al desperfecto en la tabla DesperfectoRepuesto
        INSERT INTO DesperfectoRepuesto (IdDesperfecto, IdRepuesto)
        VALUES (SCOPE_IDENTITY(), @IdRepuesto)

        -- Agregar el costo de mano de obra multiplicado por el tiempo del desperfecto al costo total
        SET @CostoTotal = @CostoTotal + (@ManoDeObra * @Tiempo) + @PrecioRepuesto

        SET @NumeroDesperfectos = @NumeroDesperfectos - 1
    END

    -- Actualizar el costo total del presupuesto
    UPDATE Presupuesto SET Total = @CostoTotal WHERE Id = @IdPresupuesto
END
GO
/****** Object:  StoredProcedure [dbo].[GetRepuestoMasUtilizadoByMarcaModelo]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRepuestoMasUtilizadoByMarcaModelo]
    @Marca NVARCHAR(50),
    @Modelo NVARCHAR(50)
AS
BEGIN
    -- Código para obtener el repuesto más utilizado por Marca/Modelo en las reparaciones realizadas
    SELECT TOP 1 R.Nombre AS DescripcionRepuesto, COUNT(DR.IdRepuesto) AS CantidadVecesUsado
    FROM Presupuesto AS P
    INNER JOIN Desperfecto AS D ON P.Id = D.IdPresupuesto
    INNER JOIN DesperfectoRepuesto AS DR ON D.Id = DR.IdDesperfecto
    INNER JOIN Repuesto AS R ON DR.IdRepuesto = R.Id
    INNER JOIN Vehiculo AS V ON P.IdVehiculo = V.Id
    WHERE V.Marca = @Marca AND V.Modelo = @Modelo
    GROUP BY R.Nombre
    ORDER BY COUNT(DR.IdRepuesto) DESC
END
GO
/****** Object:  StoredProcedure [dbo].[GetTotalPresupuestosByMarcaModelo]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTotalPresupuestosByMarcaModelo]
    @Marca NVARCHAR(50),
    @Modelo NVARCHAR(50)
AS
BEGIN
    -- Código para obtener el total de Monto Total de Presupuestos por Marca y Modelo
    SELECT SUM(P.Total) AS TotalPresupuestos
    FROM Presupuesto AS P
    INNER JOIN Vehiculo AS V ON P.IdVehiculo = V.Id
    WHERE V.Marca = @Marca AND V.Modelo = @Modelo
END
GO
/****** Object:  StoredProcedure [dbo].[GetTotalPresupuestosForAutosAndMotos]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTotalPresupuestosForAutosAndMotos]
AS
BEGIN
    -- Código para obtener la sumatoria del Monto Total de Presupuestos para Autos y Motos
    SELECT SUM(P.Total) AS TotalPresupuestos
    FROM Presupuesto AS P
END
GO
/****** Object:  StoredProcedure [dbo].[MassiveCharge]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[MassiveCharge] AS
BEGIN
    CREATE TABLE #TMP_RESPUESTO (Nombre VARCHAR(100), Precio DECIMAL(18, 6))

    BEGIN
        INSERT INTO #TMP_RESPUESTO VALUES ('B356963821', 17.61)
        INSERT INTO #TMP_RESPUESTO VALUES ('B881468337', 40.88)
        INSERT INTO #TMP_RESPUESTO VALUES ('B867719836', 87.76)
        INSERT INTO #TMP_RESPUESTO VALUES ('B397571688', 13.97)
        INSERT INTO #TMP_RESPUESTO VALUES ('B852883143', 47.97)
        INSERT INTO #TMP_RESPUESTO VALUES ('B461882670', 22.68)
        INSERT INTO #TMP_RESPUESTO VALUES ('B333520964', 82.28)
        INSERT INTO #TMP_RESPUESTO VALUES ('B388445039', 50.71)
        INSERT INTO #TMP_RESPUESTO VALUES ('B648201513', 21.83)
        INSERT INTO #TMP_RESPUESTO VALUES ('B436759416', 35.39)
        INSERT INTO #TMP_RESPUESTO VALUES ('B317533243', 22.84)
        INSERT INTO #TMP_RESPUESTO VALUES ('B666592414', 58.67)
        INSERT INTO #TMP_RESPUESTO VALUES ('B443568817', 53.83)
        INSERT INTO #TMP_RESPUESTO VALUES ('B316416378', 17.74)
        INSERT INTO #TMP_RESPUESTO VALUES ('B252543362', 16.98)
        INSERT INTO #TMP_RESPUESTO VALUES ('B453148609', 14.23)
        INSERT INTO #TMP_RESPUESTO VALUES ('B254958806', 41.19)
        INSERT INTO #TMP_RESPUESTO VALUES ('B356963821', 62.58)
        INSERT INTO #TMP_RESPUESTO VALUES ('B846487171', 92.91)
        INSERT INTO #TMP_RESPUESTO VALUES ('B397571688', 1.04)
        INSERT INTO #TMP_RESPUESTO VALUES ('B535169105', 90.14)
        INSERT INTO #TMP_RESPUESTO VALUES ('B628263302', 78.64)
        INSERT INTO #TMP_RESPUESTO VALUES ('B608816685', 93.73)
        INSERT INTO #TMP_RESPUESTO VALUES ('B660755442', 43.62)
        INSERT INTO #TMP_RESPUESTO VALUES ('B659053715', 90.59)
        INSERT INTO #TMP_RESPUESTO VALUES ('B556344166', 71.62)
        INSERT INTO #TMP_RESPUESTO VALUES ('B216140665', 93.15)
        INSERT INTO #TMP_RESPUESTO VALUES ('B843858581', 66.52)
        INSERT INTO #TMP_RESPUESTO VALUES ('B790077756', 8.91)
        INSERT INTO #TMP_RESPUESTO VALUES ('B916071768', 85.46)
        INSERT INTO #TMP_RESPUESTO VALUES ('B317533243', 7.97)
        INSERT INTO #TMP_RESPUESTO VALUES ('B343454513', 22.91)
        INSERT INTO #TMP_RESPUESTO VALUES ('B986574036', 65.10)
        INSERT INTO #TMP_RESPUESTO VALUES ('B662139869', 3.50)
        INSERT INTO #TMP_RESPUESTO VALUES ('B618792223', 6.87)
        INSERT INTO #TMP_RESPUESTO VALUES ('B578485476', 49.70)
        INSERT INTO #TMP_RESPUESTO VALUES ('B132813434', 32.58)
        INSERT INTO #TMP_RESPUESTO VALUES ('B776163235', 73.64)
        INSERT INTO #TMP_RESPUESTO VALUES ('B215908676', 92.83)
        INSERT INTO #TMP_RESPUESTO VALUES ('B871139440', 31.83)
        INSERT INTO #TMP_RESPUESTO VALUES ('B564893705', 18.91)
        INSERT INTO #TMP_RESPUESTO VALUES ('B634131771', 70.35)
        INSERT INTO #TMP_RESPUESTO VALUES ('B321187273', 91.96)
        INSERT INTO #TMP_RESPUESTO VALUES ('B444737823', 78.73)
        INSERT INTO #TMP_RESPUESTO VALUES ('B413525993', 9.93)
        INSERT INTO #TMP_RESPUESTO VALUES ('B229547877', 97.08)
        INSERT INTO #TMP_RESPUESTO VALUES ('B545788950', 11.84)
        INSERT INTO #TMP_RESPUESTO VALUES ('B658514562', 8.84)
        INSERT INTO #TMP_RESPUESTO VALUES ('B736313138', 78.47)
        INSERT INTO #TMP_RESPUESTO VALUES ('B840888802', 93.85)
        INSERT INTO #TMP_RESPUESTO VALUES ('B883572821', 21.57)
        INSERT INTO #TMP_RESPUESTO VALUES ('B493478663', 76.98)
        INSERT INTO #TMP_RESPUESTO VALUES ('B718838840', 7.41)
        INSERT INTO #TMP_RESPUESTO VALUES ('B183671709', 45.53)
        INSERT INTO #TMP_RESPUESTO VALUES ('B908384721', 14.73)
        INSERT INTO #TMP_RESPUESTO VALUES ('B566417680', 44.04)
        INSERT INTO #TMP_RESPUESTO VALUES ('B633833113', 33.28)
        INSERT INTO #TMP_RESPUESTO VALUES ('B829258206', 41.74)
        INSERT INTO #TMP_RESPUESTO VALUES ('B350041352', 85.13)
        INSERT INTO #TMP_RESPUESTO VALUES ('B548168477', 7.44)
        INSERT INTO #TMP_RESPUESTO VALUES ('B765657146', 89.79)
        INSERT INTO #TMP_RESPUESTO VALUES ('B830231322', 81.42)
        INSERT INTO #TMP_RESPUESTO VALUES ('B816385774', 9.30)
        INSERT INTO #TMP_RESPUESTO VALUES ('B857448796', 77.36)
        INSERT INTO #TMP_RESPUESTO VALUES ('B302875266', 54.89)
        INSERT INTO #TMP_RESPUESTO VALUES ('B790507487', 50.41)
        INSERT INTO #TMP_RESPUESTO VALUES ('B723629401', 65.36)
        INSERT INTO #TMP_RESPUESTO VALUES ('B595728629', 19.94)
        INSERT INTO #TMP_RESPUESTO VALUES ('B472436824', 65.69)
        INSERT INTO #TMP_RESPUESTO VALUES ('B235859870', 66.44)
        INSERT INTO #TMP_RESPUESTO VALUES ('B874178252', 42.38)
        INSERT INTO #TMP_RESPUESTO VALUES ('B777713850', 40.26)
        INSERT INTO #TMP_RESPUESTO VALUES ('B550221285', 8.72)
        INSERT INTO #TMP_RESPUESTO VALUES ('B816043247', 73.97)
        INSERT INTO #TMP_RESPUESTO VALUES ('B607313788', 15.95)
        INSERT INTO #TMP_RESPUESTO VALUES ('B396482694', 45.17)
        INSERT INTO #TMP_RESPUESTO VALUES ('B504021331', 24.52)
        INSERT INTO #TMP_RESPUESTO VALUES ('B651475349', 86.77)
        INSERT INTO #TMP_RESPUESTO VALUES ('B470409863', 11.81)
        INSERT INTO #TMP_RESPUESTO VALUES ('B264135435', 62.58)
        INSERT INTO #TMP_RESPUESTO VALUES ('B755636151', 33.88)
        INSERT INTO #TMP_RESPUESTO VALUES ('B382183955', 0.92)
        INSERT INTO #TMP_RESPUESTO VALUES ('B667316286', 0.29)
        INSERT INTO #TMP_RESPUESTO VALUES ('B783117048', 41.57)
        INSERT INTO #TMP_RESPUESTO VALUES ('B812952354', 86.25)
        INSERT INTO #TMP_RESPUESTO VALUES ('B621838237', 80.54)
        INSERT INTO #TMP_RESPUESTO VALUES ('B665465223', 53.69)
        INSERT INTO #TMP_RESPUESTO VALUES ('B881682635', 64.78)
        INSERT INTO #TMP_RESPUESTO VALUES ('B646289861', 72.01)
        INSERT INTO #TMP_RESPUESTO VALUES ('B852115667', 48.73)
        INSERT INTO #TMP_RESPUESTO VALUES ('B144635415', 34.23)
        INSERT INTO #TMP_RESPUESTO VALUES ('B874863828', 24.70)
        INSERT INTO #TMP_RESPUESTO VALUES ('B333841476', 41.57)
        INSERT INTO #TMP_RESPUESTO VALUES ('B587386017', 45.27)
        INSERT INTO #TMP_RESPUESTO VALUES ('B874270576', 42.38)
        INSERT INTO #TMP_RESPUESTO VALUES ('B300733136', 25.55)
        INSERT INTO #TMP_RESPUESTO VALUES ('B611446656', 60.12)
        INSERT INTO #TMP_RESPUESTO VALUES ('B801300387', 61.04)
        INSERT INTO #TMP_RESPUESTO VALUES ('B845153562', 60.09)
        INSERT INTO #TMP_RESPUESTO VALUES ('B943846621', 37.05)
    END
 -- Acumular el valor de repuestos duplicados en la tabla definitiva
    UPDATE R
    SET Precio = R.Precio + T.Precio
    FROM Repuesto R
    INNER JOIN #TMP_RESPUESTO T ON R.Nombre = T.Nombre

    -- Cargar en la tabla definitiva "Repuesto" aquellos registros que existen en la tabla temporal "#TMP_RESPUESTO" y cuyo valor del repuesto sea menor a $20
    INSERT INTO Repuesto(Nombre, Precio)
    SELECT Nombre, Precio
    FROM #TMP_RESPUESTO
    WHERE Precio < 20

    -- Reportar aquellos repuestos cuyo valor es mayor o igual a $20 y no fueron insertados o actualizados en la tabla definitiva
    SELECT Nombre, Precio
    FROM #TMP_RESPUESTO
    WHERE Precio >= 20

    DROP TABLE #TMP_RESPUESTO
END
GO
/****** Object:  StoredProcedure [dbo].[ObtenerPresupuestoPorPatente]    Script Date: 1/8/2023 18:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ObtenerPresupuestoPorPatente]
    @Patente NVARCHAR(10)
AS
BEGIN
    DECLARE @IdVehiculo BIGINT;
	-- Asignar el valor del ID del vehículo mediante la consulta
	SELECT @IdVehiculo = Id FROM Vehiculo WHERE Patente = @Patente;


    IF @IdVehiculo IS NULL
    BEGIN
        RETURN
    END

    -- Buscar presupuesto asociado al vehículo
    SELECT
        p.Nombre,
        p.Apellido,
        p.Email,
        p.Total,
        d.Id AS DesperfectoId,
        d.Descripcion,
        d.ManoDeObra,
        d.Tiempo,
        r.Nombre AS RepuestoNombre,
        r.Precio AS RepuestoPrecio
    FROM Presupuesto p
    INNER JOIN Desperfecto d ON p.Id = d.IdPresupuesto
    LEFT JOIN DesperfectoRepuesto dr ON d.Id = dr.IdDesperfecto
    LEFT JOIN Repuesto r ON dr.IdRepuesto = r.Id
    WHERE p.IdVehiculo = @IdVehiculo
END
GO
USE [master]
GO
ALTER DATABASE [DiWorks] SET  READ_WRITE 
GO
