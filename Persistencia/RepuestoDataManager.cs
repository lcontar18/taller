namespace Taller.Persistencia
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Taller.Modelos.Models;

    public class RepuestoDataManager
    {
        private readonly string connectionString;

        public RepuestoDataManager(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DataSet GetFromDataBase(string tableName, int id)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                string query = $"SELECT * FROM {tableName} WHERE Id = @Id";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Id", id);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, tableName);
                return dataSet;
            }
        }

        public DataSet GetAllFromDataBase(string tableName)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                SqlCommand command = new SqlCommand($"SELECT * FROM {tableName}", connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet, tableName);
                return dataSet;
            }
        }

        public void UpdateToDataBase(DataSet dataSet)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                SqlCommand selectCommand = new SqlCommand("SELECT * FROM Repuesto", connection);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder();
                commandBuilder.DataAdapter = new SqlDataAdapter(selectCommand);

                SqlDataAdapter dataAdapter = new SqlDataAdapter(selectCommand);
                dataAdapter.Update(dataSet, "Repuesto");
            }
        }

        public List<Tuple<string, string, string, int>> GetMostUsedRepuestoByMarcaModelo()
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                // Crear el Stored Procedure que obtiene el repuesto m�s utilizado por Marca/Modelo en las reparaciones realizadas
                SqlCommand cmd = new SqlCommand("dbo.GetMostUsedRepuestoByMarcaModelo", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                List<Tuple<string, string, string, int>> result = new List<Tuple<string, string, string, int>>();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string marca = reader.GetString(0);
                        string modelo = reader.GetString(1);
                        string repuestoDescripcion = reader.GetString(2);
                        int cantidadVecesUsado = reader.GetInt32(3);
                        result.Add(new Tuple<string, string, string, int>(marca, modelo, repuestoDescripcion, cantidadVecesUsado));
                    }
                }

                connection.Close();

                return result;
            }
        }

        // M�todo para obtener el promedio del Monto Total de Presupuestos por Marca/Modelo
        public List<Tuple<string, string, decimal>> GetTotalPresupuestosByMarcaModelo()
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                // Crear el Stored Procedure que obtiene el promedio del Monto Total de Presupuestos por Marca/Modelo
                SqlCommand cmd = new SqlCommand("dbo.GetAverageMontoTotalPresupuestosByMarcaModelo", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                List<Tuple<string, string, decimal>> result = new List<Tuple<string, string, decimal>>();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string marca = reader.GetString(0);
                        string modelo = reader.GetString(1);
                        decimal promedioMontoTotal = reader.GetDecimal(2);
                        result.Add(new Tuple<string, string, decimal>(marca, modelo, promedioMontoTotal));
                    }
                }

                connection.Close();

                return result;
            }
        }

        // M�todo para obtener la sumatoria del Monto Total de Presupuestos para Autos y para Motos
        public Tuple<decimal, decimal> GetTotalPresupuestosForAutosAndMotos()
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                // Crear el Stored Procedure que obtiene la sumatoria del Monto Total de Presupuestos para Autos y Motos
                SqlCommand cmd = new SqlCommand("dbo.GetSumMontoTotalPresupuestosForAutosAndMotos", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                decimal sumMontoTotalAutos = 0;
                decimal sumMontoTotalMotos = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        sumMontoTotalAutos = reader.GetDecimal(0);
                        sumMontoTotalMotos = reader.GetDecimal(1);
                    }
                }

                connection.Close();

                return new Tuple<decimal, decimal>(sumMontoTotalAutos, sumMontoTotalMotos);
            }
        }

        public void AgregarVehiculo(string marca, string modelo, TipoVehiculo tipoVehiculo, string patente = null, string cilindrada = null, AutomovilTipo? tipoAutomovil = null, short? cantidadPuertas = null)
        {
            if (string.IsNullOrEmpty(marca) || string.IsNullOrEmpty(modelo))
            {
                throw new ArgumentException("Marca y modelo son campos obligatorios.");
            }

            if (tipoVehiculo == TipoVehiculo.Moto && string.IsNullOrEmpty(cilindrada))
            {
                throw new ArgumentException("La cilindrada es un campo obligatorio para las motos.");
            }

            if (tipoVehiculo == TipoVehiculo.Automovil && (tipoAutomovil == null || cantidadPuertas == null))
            {
                throw new ArgumentException("El tipo y la cantidad de puertas son campos obligatorios para los automóviles.");
            }

            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                // Insertar el registro en la tabla Vehiculo
                SqlCommand commandVehiculo = new SqlCommand("INSERT INTO Vehiculo (Marca, Modelo, Patente) " + "VALUES (@Marca, @Modelo, @Patente); SELECT SCOPE_IDENTITY();", connection);
                commandVehiculo.Parameters.AddWithValue("@Marca", marca);
                commandVehiculo.Parameters.AddWithValue("@Modelo", modelo);
                commandVehiculo.Parameters.AddWithValue("@Patente", patente);
                long vehiculoId = Convert.ToInt64(commandVehiculo.ExecuteScalar());

                if (tipoVehiculo == TipoVehiculo.Moto)
                {
                    // Insertar el registro en la tabla Moto
                    SqlCommand commandMoto = new SqlCommand("INSERT INTO Moto (IdVehiculo, Cilindrada) VALUES (@IdVehiculo, @Cilindrada)", connection);
                    commandMoto.Parameters.AddWithValue("@IdVehiculo", vehiculoId);
                    commandMoto.Parameters.AddWithValue("@Cilindrada", cilindrada);
                    commandMoto.ExecuteNonQuery();
                }
                else if (tipoVehiculo == TipoVehiculo.Automovil)
                {
                    short tipoAutomovilValue = tipoAutomovil.HasValue ? (short)tipoAutomovil.Value : (short)0;
                    // Insertar el registro en la tabla Automovil
                    SqlCommand commandAutomovil = new SqlCommand(
                        "INSERT INTO Automovil (IdVehiculo, Tipo, CantidadPuertas) " + "VALUES (@IdVehiculo, @Tipo, @CantidadPuertas)", connection);
                    commandAutomovil.Parameters.AddWithValue("@IdVehiculo", vehiculoId);
                    commandAutomovil.Parameters.AddWithValue("@Tipo", tipoAutomovilValue);
                    commandAutomovil.Parameters.AddWithValue("@CantidadPuertas", cantidadPuertas);
                    commandAutomovil.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        public void GenerarPresupuesto(string nombre, string apellido, string email, string patente)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("GenerarPresupuesto", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@Nombre", nombre);
                command.Parameters.AddWithValue("@Apellido", apellido);
                command.Parameters.AddWithValue("@Email", email);
                command.Parameters.AddWithValue("@Patente", patente);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public Presupuesto ObtenerPresupuestoPorPatente(string patente)
        {
            using (SqlConnection connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("ObtenerPresupuestoPorPatente", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Patente", patente);

                SqlDataReader reader = command.ExecuteReader();
                Presupuesto presupuesto = null;
                if (reader.Read())
                {
                    presupuesto = new Presupuesto
                    {
                        Nombre = reader.GetString(reader.GetOrdinal("Nombre")),
                        Apellido = reader.GetString(reader.GetOrdinal("Apellido")),
                        Email = reader.GetString(reader.GetOrdinal("Email")),
                        Total = reader.GetDecimal(reader.GetOrdinal("Total")),
                        DetallesPresupuesto = new List<DetallesPresupuesto>(),
                    };

                    do
                    {
                        DetallesPresupuesto detalle = new DetallesPresupuesto
                        {
                            DesperfectoId = reader.GetInt64(reader.GetOrdinal("DesperfectoId")),
                            Descripcion = reader.GetString(reader.GetOrdinal("Descripcion")),
                            ManoDeObra = reader.GetDecimal(reader.GetOrdinal("ManoDeObra")),
                            Tiempo = reader.GetInt32(reader.GetOrdinal("Tiempo")),
                            RepuestoNombre = reader.GetString(reader.GetOrdinal("RepuestoNombre")),
                            RepuestoPrecio = reader.GetDecimal(reader.GetOrdinal("RepuestoPrecio")),
                        };
                        presupuesto.DetallesPresupuesto.Add(detalle);
                    }
                    while (reader.Read());
                }

                connection.Close();
                return presupuesto;
            }
        }
    }
}
