CREATE PROCEDURE dbo.GetTotalPresupuestosByMarcaModelo
    @Marca NVARCHAR(50),
    @Modelo NVARCHAR(50)
AS
BEGIN
    -- C�digo para obtener el total de Monto Total de Presupuestos por Marca y Modelo
    SELECT SUM(P.Total) AS TotalPresupuestos
    FROM Presupuesto AS P
    INNER JOIN Vehiculo AS V ON P.IdVehiculo = V.Id
    WHERE V.Marca = @Marca AND V.Modelo = @Modelo
END
