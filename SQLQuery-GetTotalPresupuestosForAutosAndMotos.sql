CREATE PROCEDURE dbo.GetTotalPresupuestosForAutosAndMotos
AS
BEGIN
    -- C�digo para obtener la sumatoria del Monto Total de Presupuestos para Autos y Motos
    SELECT SUM(P.Total) AS TotalPresupuestos
    FROM Presupuesto AS P
END
