CREATE PROCEDURE ObtenerPresupuestoPorPatente
    @Patente NVARCHAR(10)
AS
BEGIN
    -- Obtener el ID del veh�culo en base a la patente
    DECLARE @IdVehiculo BIGINT
    SELECT @IdVehiculo = Id FROM Vehiculo WHERE Patente = @Patente

    IF @IdVehiculo IS NULL
    BEGIN
        RETURN
    END

    -- Buscar presupuesto asociado al veh�culo
    DECLARE @IdPresupuesto BIGINT
    SELECT @IdPresupuesto = Id FROM Presupuesto WHERE IdVehiculo = @IdVehiculo

    IF @IdPresupuesto IS NULL
    BEGIN
        RETURN
    END

    -- Obtener los detalles del veh�culo
    SELECT
        v.Marca,
        v.Modelo,
        a.Tipo,
        p.Nombre,
        p.Apellido,
        p.Email,
        p.Total,
        d.Id AS DesperfectoId,
        d.Descripcion,
        d.ManoDeObra,
        d.Tiempo,
        r.Nombre AS RepuestoNombre,
        r.Precio AS RepuestoPrecio
    FROM Presupuesto p
    INNER JOIN Vehiculo v ON p.IdVehiculo = v.Id
    LEFT JOIN Moto m ON v.Id = m.IdVehiculo
    LEFT JOIN Automovil a ON v.Id = a.IdVehiculo
    INNER JOIN Desperfecto d ON p.Id = d.IdPresupuesto
    LEFT JOIN DesperfectoRepuesto dr ON d.Id = dr.IdDesperfecto
    LEFT JOIN Repuesto r ON dr.IdRepuesto = r.Id
    WHERE p.Id = @IdPresupuesto
END
